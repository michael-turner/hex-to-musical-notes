#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
int main(int argc, char** argv) {
    int i;
    int k;
    int j;
    int t;
    char s[44] = { 0x52, 0x49, 0x46, 0x46, 0x00, 0x00, 0x00, 0x00, 0x57, 0x41, 0x56, 0x45, 0x66, 0x6D, 0x74, 0x20, 0x10, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x44, 0xAC, 0x00, 0x00, 0x44, 0xAC, 0x00, 0x00, 0x01, 0x00, 0x08, 0x00, 0x64, 0x61, 0x74, 0x61, 0x00, 0x00, 0x00, 0x00 } ;
    for (i = 0; i < 4; i++) putc (s[i],stdout);
    int length=strlen(argv[1]);
    int length_plus_36=length+36;
    putc (length_plus_36&0xff,stdout);
    putc (length_plus_36&0xff00/0x100,stdout);
    putc (length_plus_36&0xff0000/0x10000,stdout);
    putc (length_plus_36&0xff000000/0x1000000,stdout);
    for (i = 8; i < 40; i++) putc (s[i],stdout);
    putc (length&0xff,stdout);
    putc (length&0xff00/0x100,stdout);
    putc (length&0xff0000/0x10000,stdout);
    putc (length&0xff000000/0x1000000,stdout);
    float f;
    i=1; {
        for(k = 0; k < strlen(argv[i]); k++) {
            if(argv[i][k]>=0x30&&argv[i][k]<=0x39) t=argv[i][k]-0x30;
            if(argv[i][k]>=0x41&&argv[i][k]<=0x5a) t=argv[i][k]-0x40;
            if(argv[i][k]>=0x61&&argv[i][k]<=0x7a) t=argv[i][k]-0x60;
            f=440*pow(2.0,(t+3)/12.0);
            for(j = 0; j < 11025; j++) {
                putc( (char) ( (cos(2.0*M_PI*j/11025) *-0.5+0.5) * (sin (2.0*M_PI*f*j/44100) ) * 440.0 * 127.0 / f + 127.5 ),stdout );
            }
        }
    }
    return 0;
}

