# hex-to-musical-notes

dependencies: mplayer vim git build-essential

git clone https://gitlab.com/michael-turner/hex-to-musical-notes

cd hex-to-musical-notes

gcc hextones.c -o hextones -lm

shasum -a 256 | xargs ./hextones > output.wav

mplayer output.wav
